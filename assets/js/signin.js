function initApp() {
    var database = firebase.database().ref();
    var userlist = database.child('userlist');
    var chatroom = database.child('chatroom');
    var accountL = document.getElementById("inputEmail");
    var pwdL = document.getElementById("inputPassword");
    var loginBtn = document.getElementById("loginbtn");
    var logingooglebtn = document.getElementById("logingooglebtn");
    var registerbtn = document.getElementById("registerbtn");


    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            window.location.href = "room.html";
        }
    });

    //信箱登入
    loginBtn.addEventListener("click", function () {

        firebase.auth().signInWithEmailAndPassword(accountL.value, pwdL.value).then(function () {
            if (Notification.permission == "granted") {
                var notify = {
                    body: 'Login Successfully!',
                    icon: "https://drive.google.com/open?id=1YdQPZ8qxX7g5DTVw5efU7A7vpqwLbeuM"
                };
                var new_note = new Notification("PingMe", notify);
            }
            window.location.href = "room.html";
        }).catch(function (error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            console.log(errorMessage);
        })
    }, false);

    //google登入
    logingooglebtn.addEventListener('click', function () {
        var provider = new firebase.auth.GoogleAuthProvider();
        provider.addScope('https://www.googleapis.com/auth/contacts.readonly');
        firebase.auth().signInWithPopup(provider).then(function (result) {

            var token = result.credential.accessToken;
            var user = result.user;
            var UserInList = 0;

            userlist.once('value')
                .then(function (snapshot) {
                    snapshot.forEach(function (childshot) {
                        var data = childshot.val();
                        console.log(data);
                        if (data.useremail == user.email) {
                            console.log("user in the list");
                            UserInList = 1;
                        }
                    });

                    console.log(UserInList);
                    if (UserInList == 0) {
                        UserInList = 1
                        var data = {
                            username: user.displayName,
                            useremail: user.email
                        }
                        userlist.push(data);
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });

                if (Notification.permission == "granted") {
                    var notify = {
                        body: 'Login Successfully!',
                        icon: "https://drive.google.com/open?id=1YdQPZ8qxX7g5DTVw5efU7A7vpqwLbeuM"
                    };
                    var new_note = new Notification("PingMe", notify);
                }
            //window.location.href = "room.html";
        }).catch(function (error) {
            console.log(error.message);
        });
    });

    //register redirection
    registerbtn.addEventListener("click", function () {
        window.location.href = "register.html";
    }, false);
}

$(function () {

    var $body = $('body');

    // Menu.
    $('#menu')
        .append('<a href="#menu" class="close"></a>')
        .appendTo($body)
        .panel({
            delay: 500,
            hideOnClick: true,
            hideOnSwipe: true,
            resetScroll: true,
            resetForms: true,
            side: 'right'
        });

});

window.onload = function () {
    initApp();
};