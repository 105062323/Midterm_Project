$(function () {
    var $body = $('body');
    // Menu.
    $('#menu')
        .append('<a href="#menu" class="close"></a>')
        .appendTo($body)
        .panel({
            delay: 500,
            hideOnClick: true,
            hideOnSwipe: true,
            resetScroll: true,
            resetForms: true,
            side: 'right'
        });

    /*user list */
    var w = $("#mwt_slider_content").width();
    $('#mwt_slider_content').css('height', ($(window).height() - 20) + 'px');

    $("#mwt_fb_tab").mouseover(function () {
        if ($("#mwt_mwt_slider_scroll").css('left') == '-' + w + 'px') {
            $("#mwt_mwt_slider_scroll").animate({ left: '0px' }, 600, 'swing');
        }
    });


    $("#mwt_slider_content").mouseleave(function () {
        $("#mwt_mwt_slider_scroll").animate({ left: '-' + w + 'px' }, 600, 'swing');
    });

    //Var setting
    var $name = $('#userid'),
        $content = $('#content'),
        $btn = $('#btn'),
        $loginbtn = $('#loginbtn'),
        $show = $('#show'),
        $roomid = $('#roomid'),
        $room_lobby = $('#Lobby'),
        $room_other = $('#otherroom'),
        $room_my = $('#myroom'),
        database = firebase.database().ref(),
        userlist = database.child('userlist'),
        chatroom = database.child('chatroom'),
        useronline = database.child('useronline'),
        roomid = $roomid[0].innerHTML,
        logoutbtn = $('#logout'),
        useremail = false,
        username = false;



    /*認證*/
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            //login success & get user's information
            console.log("User is logined", user);
            useremail = user.email;

            /*show logout button */
            document.getElementById('logout').innerHTML = "Logout";
            /*Logout */
            logoutbtn.on("click", function () {
                firebase.auth().signOut().then(function () {
                    useronline.once('value')
                        .then(function (snapshot) {
                            snapshot.forEach(function (childshot) {
                                var data = childshot.val();
                                if (data.useremail == useremail) {
                                    useronline.child(childshot).remove();
                                    break;
                                }
                            });

                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                    if (Notification.permission == "granted") {
                        var notify = {
                            body: 'Logout Successfully!',
                            icon: "https://drive.google.com/open?id=1YdQPZ8qxX7g5DTVw5efU7A7vpqwLbeuM"
                        };
                        var new_note = new Notification("PingMe", notify);
                    }
                    window.location.href = "index.html";
                }).catch(function (error) {
                    console.log("User sign out failed!");
                })
            });

            var user_data_list = [];
            var first_count_of_user = 0;
            var second_count_of_user = 0;
            var str_before = "<a href='#'>";
            var str_after = "</a>";
            /*get user name*/
            userlist.once('value')
                .then(function (snapshot) {

                    user_data_list = [];
                    first_count_of_user = 0;
                    second_count_of_user = 0;

                    snapshot.forEach(function (childshot) {
                        var data = childshot.val();
                        user_data_list[user_data_list.length] = str_before + data.username + str_after;
                        first_count_of_user += 1


                        /*Is current user */
                        if (data.useremail == useremail) {
                            $loginbtn[0].innerHTML = data.username;
                            $loginbtn[0].href = "profile.html";
                            username = data.username;
                        }
                    });

                    document.getElementById('left_slider').innerHTML = user_data_list.join('');

                    userlist.child('' + roomid).on('child_added', function (data) {
                        second_count_of_user += 1;
                        if (second_count_of_user > first_count_of_user) {
                            var childData = data.val();
                            user_data_list[user_data_list.length] = str_before + childData.username + str_after;
                            document.getElementById('left_slider').innerHTML = user_data_list.join('');
                        }
                    });

                })
                .catch(function (error) {
                    console.log(error);
                });

            /*check if user online*/
            var inList = 0;
            useronline.once('value')
                .then(function (snapshot) {
                    inList = 0;
                    snapshot.forEach(function (childshot) {
                        var data = childshot.val();
                        if (data.useremail == useremail) {
                            inList = 1;
                            console.log(inList);
                        }
                    });

                    console.log(inList);
                    if (inList == 0) {
                        inList = 1
                        var online_data = {
                            useremail: useremail
                        }
                        useronline.push(online_data);
                    }
                }).then(function () {
                    console.log(inList);
                    if (inList == 0) {
                        inList = 1
                        var online_data = {
                            useremail: useremail
                        }
                        useronline.push(online_data);
                    }
                });



            /*Send the text*/
            $content.on('keydown', function (e) {
                if (e.keyCode == 13) { //push enter
                    var text = $(this).val();
                    if (text !== "") { //迴避空字串
                        /*get current time*/
                        text = text.replace(/</g, ">");
                        var currentdate = new Date();
                        var datetime =
                            currentdate.getFullYear() + "/" +
                            (currentdate.getMonth() + 1) + "/" +
                            currentdate.getDate() + "_" +
                            currentdate.getHours() + ":" +
                            currentdate.getMinutes() + ":" +
                            currentdate.getSeconds();
                        var Data = {
                            name: $loginbtn[0].innerHTML,
                            content: text,
                            time: datetime
                        };
                        chatroom.child(roomid).push(Data);
                        $(this).val('');
                    }
                }
            });
            $btn.on('click', function () {
                var currentdate = new Date();
                var datetime =
                    currentdate.getFullYear() + "/" +
                    (currentdate.getMonth() + 1) + "/" +
                    currentdate.getDate() + "_" +
                    currentdate.getHours() + ":" +
                    currentdate.getMinutes() + ":" +
                    currentdate.getSeconds();

                var text = $content.val();
                text = text.replace(/</g, ">");
                var Data = {
                    name: $loginbtn[0].innerHTML,
                    content: text,
                    time: datetime
                };
                chatroom.child(roomid).push(Data);
                $content.val('');
            });


            /*Text var */
            var str_before_username = "<div class='my-1 p-3 bg-white rounded box-shadow'><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
            var str_after_content = "</p></div></div>\n";
            var data_list = [];
            var first_count = 0;
            var second_count = 0;

            /*Change the room: lobby, myroom, otherroom */
            $room_my.on('click', function () {

                /*Initialization */
                roomid = username;
                $roomid[0].innerHTML = roomid;
                data_list = [];
                first_count = 0;
                second_count = 0;
                console.log(roomid);

                chatroom.child('' + roomid).once('value')
                    .then(function (snapshot) {
                        snapshot.forEach(function (childshot) {

                            var data = childshot.val();
                            data_list[data_list.length] = str_before_username + data.name + " :  " + data.time + "</strong>" + data.content + str_after_content;
                            first_count += 1
                        });

                        document.getElementById('show').innerHTML = data_list.join('');

                        chatroom.child('' + roomid).on('child_added', function (data) {
                            second_count += 1;
                            if (second_count > first_count) {
                                var childData = data.val();
                                data_list[data_list.length] = str_before_username + childData.name + " :  " + childData.time + "</strong>" + childData.content + str_after_content;
                                document.getElementById('show').innerHTML = data_list.join('');
                            }
                        });
                    })
                    .catch(e => console.log(e.message));
            });
            $room_other.on('click', function () {

                var name = prompt("输入想要進入的user name", "");

                if (name) {
                    /*check the ID is in the list */
                    userlist.once('value')
                        .then(function (snapshot) {
                            snapshot.forEach(function (childshot) {
                                var data = childshot.val();
                                if (data.username == name) {
                                    roomid = name;
                                    $roomid[0].innerHTML = roomid;
                                    data_list = [];
                                    first_count = 0;
                                    second_count = 0;
                                    console.log(roomid);

                                    chatroom.child('' + roomid).once('value')
                                        .then(function (snapshot) {
                                            snapshot.forEach(function (childshot) {

                                                var data = childshot.val();
                                                data_list[data_list.length] = str_before_username + data.name + " :  " + data.time + "</strong>" + data.content + str_after_content;
                                                first_count += 1
                                            });

                                            document.getElementById('show').innerHTML = data_list.join('');

                                            chatroom.child('' + roomid).on('child_added', function (data) {
                                                second_count += 1;
                                                if (second_count > first_count) {
                                                    var childData = data.val();
                                                    data_list[data_list.length] = str_before_username + childData.name + " :  " + childData.time + "</strong>" + childData.content + str_after_content;
                                                    document.getElementById('show').innerHTML = data_list.join('');
                                                }
                                            });
                                        })
                                        .catch(e => console.log(e.message));
                                }
                            });
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                }
            });
            $room_lobby.on('click', function () {

                /*Initialization */
                roomid = "Lobby";
                $roomid[0].innerHTML = roomid;
                data_list = [];
                first_count = 0;
                second_count = 0;
                console.log(roomid);

                chatroom.child('' + roomid).once('value')
                    .then(function (snapshot) {
                        snapshot.forEach(function (childshot) {

                            var data = childshot.val();
                            data_list[data_list.length] = str_before_username + data.name + " :  " + data.time + "</strong>" + data.content + str_after_content;
                            first_count += 1
                        });

                        document.getElementById('show').innerHTML = data_list.join('');

                        chatroom.child('' + roomid).on('child_added', function (data) {
                            second_count += 1;
                            if (second_count > first_count) {
                                var childData = data.val();
                                data_list[data_list.length] = str_before_username + childData.name + " :  " + childData.time + "</strong>" + childData.content + str_after_content;
                                document.getElementById('show').innerHTML = data_list.join('');
                            }
                        });
                    })
                    .catch(e => console.log(e.message));
            });


            /*renew the message */
            chatroom.child('' + roomid).once('value')
                .then(function (snapshot) {
                    snapshot.forEach(function (childshot) {

                        var data = childshot.val();
                        data_list[data_list.length] = str_before_username + data.name + " :  " + data.time + "</strong>" + data.content + str_after_content;
                        first_count += 1
                    });

                    document.getElementById('show').innerHTML = data_list.join('');

                    chatroom.child('' + roomid).on('child_added', function (data) {
                        second_count += 1;
                        if (second_count > first_count) {
                            var childData = data.val();
                            data_list[data_list.length] = str_before_username + childData.name + " :  " + childData.time + "</strong>" + childData.content + str_after_content;
                            document.getElementById('show').innerHTML = data_list.join('');
                        }
                    });
                })
                .catch(e => console.log(e.message));

        } else {
            console.log("User is not logined yet.");
            /*
            var transport = confirm("Go to signin page?");
            if (transport) {
                document.location.href = "signin.html";
            } else {
                document.location.href = "index.html";
            }
            */
        }
    });

    /*
        <div class='my-3 p-3 bg-white rounded box-shadow'>
            <div class='media text-muted pt-3' style='background-image:url(images/176.jpg)'>
                <img src='images/176.jpg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'>
                <p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'>
                    <strong class='d-block text-gray-dark'>
                    username
                    </strong>
                    content
                </p>
            </div>
        </div>
    */
});