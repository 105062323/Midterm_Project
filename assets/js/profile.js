var settings = {
    banner: {
        // Indicators (= the clickable dots at the bottom).
        indicators: true,
        speed: 1500,
        delay: 5000,
        parallax: 0.25
    }
};
var global_user = false;
var global_key = false;

(function ($) {

    skel.breakpoints({
        xlarge: '(max-width: 1680px)',
        large: '(max-width: 1280px)',
        medium: '(max-width: 980px)',
        small: '(max-width: 736px)',
        xsmall: '(max-width: 480px)'
    });

    $.fn._parallax = (skel.vars.browser == 'ie' || skel.vars.mobile) ? function () { return $(this) } : function (intensity) {

        var $window = $(window),
            $this = $(this);

        if (this.length == 0 || intensity === 0)
            return $this;

        if (this.length > 1) {
            for (var i = 0; i < this.length; i++)
                $(this[i])._parallax(intensity);
            return $this;
        }

        if (!intensity)
            intensity = 0.25;

        $this.each(function () {
            var $t = $(this),
                on, off;
            on = function () {
                $t.css('background-position', 'center 100%, center 100%, center 0px');
                $window
                    .on('scroll._parallax', function () {
                        var pos = parseInt($window.scrollTop()) - parseInt($t.position().top);
                        $t.css('background-position', 'center ' + (pos * (-1 * intensity)) + 'px');
                    });
            };

            off = function () {
                $t
                    .css('background-position', '');
                $window
                    .off('scroll._parallax');
            };
            skel.on('change', function () {
                if (skel.breakpoint('medium').active)
                    (off)();
                else
                    (on)();
            });
        });

        $window
            .off('load._parallax resize._parallax')
            .on('load._parallax resize._parallax', function () {
                $window.trigger('scroll');
            });
        return $(this);
    };

    $.fn._slider = function (options) {

        var $window = $(window),
            $this = $(this);
        if (this.length == 0)
            return $this;
        if (this.length > 1) {
            for (var i = 0; i < this.length; i++)
                $(this[i])._slider(options);
            return $this;
        }
        // Vars.
        var current = 0,
            pos = 0,
            lastPos = 0,
            slides = [],
            indicators = [],
            $indicators,
            $slides = $this.children('article'),
            intervalId,
            isLocked = false,
            i = 0;

        // Turn off indicators if we only have one slide.
        if ($slides.length == 1)
            options.indicators = false;

        // Functions.
        $this._switchTo = function (x, stop) {

            if (isLocked || pos == x)
                return;

            isLocked = true;

            if (stop)
                window.clearInterval(intervalId);

            // Update positions.
            lastPos = pos;
            pos = x;

            // Hide last slide.
            slides[lastPos].removeClass('top');

            if (options.indicators)
                indicators[lastPos].removeClass('visible');

            // Show new slide.
            slides[pos].addClass('visible').addClass('top');

            if (options.indicators)
                indicators[pos].addClass('visible');

            // Finish hiding last slide after a short delay.
            window.setTimeout(function () {

                slides[lastPos].addClass('instant').removeClass('visible');

                window.setTimeout(function () {

                    slides[lastPos].removeClass('instant');
                    isLocked = false;
                }, 100);
            }, options.speed);

        };

        // Indicators.
        if (options.indicators)
            $indicators = $('<ul class="indicators"></ul>').appendTo($this);

        // Slides.
        $slides
            .each(function () {

                var $slide = $(this),
                    $img = $slide.find('img');

                // Slide.
                $slide
                    .css('background-image', 'url("' + $img.attr('src') + '")')
                    .css('background-position', ($slide.data('position') ? $slide.data('position') : 'center'));

                // Add to slides.
                slides.push($slide);

                // Indicators.
                if (options.indicators) {

                    var $indicator_li = $('<li>' + i + '</li>').appendTo($indicators);

                    // Indicator.
                    $indicator_li
                        .data('index', i)
                        .on('click', function () {
                            $this._switchTo($(this).data('index'), true);
                        });

                    // Add to indicators.
                    indicators.push($indicator_li);

                }

                i++;

            })
            ._parallax(options.parallax);

        // Initial slide.
        slides[pos].addClass('visible').addClass('top');

        if (options.indicators)
            indicators[pos].addClass('visible');

        // Bail if we only have a single slide.
        if (slides.length == 1)
            return;

        // Main loop.
        intervalId = window.setInterval(function () {

            current++;

            if (current >= slides.length)
                current = 0;

            $this._switchTo(current);

        }, options.delay);

    };

    $(function () {

        var $window = $(window),
            $body = $('body'),
            $header = $('#header'),
            $banner = $('.banner');

        // Disable animations/transitions until the page has loaded.
        $body.addClass('is-loading');

        $window.on('load', function () {
            window.setTimeout(function () {
                $body.removeClass('is-loading');
            }, 100);
        });

        // Prioritize "important" elements on medium.
        skel.on('+medium -medium', function () {
            $.prioritize(
                '.important\\28 medium\\29',
                skel.breakpoint('medium').active
            );
        });

        // Banner.
        $banner._slider(settings.banner);

        // Menu.
        $('#menu')
            .append('<a href="#menu" class="close"></a>')
            .appendTo($body)
            .panel({
                delay: 500,
                hideOnClick: true,
                hideOnSwipe: true,
                resetScroll: true,
                resetForms: true,
                side: 'right'
            });

        // Header.
        if (skel.vars.IEVersion < 9)
            $header.removeClass('alt');

        if ($banner.length > 0 &&
            $header.hasClass('alt')) {

            $window.on('resize', function () { $window.trigger('scroll'); });

            $banner.scrollex({
                bottom: $header.outerHeight(),
                terminate: function () { $header.removeClass('alt'); },
                enter: function () { $header.addClass('alt'); },
                leave: function () {
                    $header.removeClass('alt');
                    $header.addClass('reveal');
                }
            });

        }

    });

    /*var setting*/
    var database = firebase.database().ref(),
        storage = firebase.storage().ref(),
        user_image = storage.child('images'),
        userlist = database.child('userlist'),
        btn = $('#loginbtn'),
        $show_username = $('#show_username'),
        $show_userintro = $('#show_userintro'),
        $show_userimage = $('#user_image'),
        $change = $('#change_profile'),
        $profile = $('#profile'),
        $save = $('#save_profile'),
        useremail = false,
        username = false;

    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {

            useremail = user.email;
            /*show logout button */
            document.getElementById('logout').innerHTML = "Logout";
            var logoutbtn = $('#logout');
            /*Logout */
            logoutbtn.on("click", function () {
                firebase.auth().signOut().then(function () {
                    if (Notification.permission == "granted") {
                        var storageRef = firebase.storage().ref();
                        var pathReference = storageRef.child('images/Stark.ico');

                        pathReference.getDownloadURL().then(function (url) {
                            var notify = {
                                body: 'Logout Successfully!',
                                icon: url
                            };
                            var new_note = new Notification("PingMe", notify);
                        });
                    }
                    window.location.href = "index.html";
                }).catch(function (error) {
                    console.log("User sign out failed!");
                })
            });

            /*find user name and set to login button*/
            userlist.once('value')
                .then(function (snapshot) {
                    snapshot.forEach(function (childshot) {
                        var data = childshot.val();
                        if (data.useremail == useremail) {
                            console.log(data);
                            global_key = childshot;
                            global_user = data;

                            username = data.username;
                            btn.html(username);
                            btn[0].href = "profile.html";
                            $show_username.html(username);
                            //$show_userimage.attr("src", "url(data.userimage)");

                            var input = global_user.useremail.replace("@", "-");
                            input = input.replace(".", "-");
                            input = input.replace(".", "-");
                            console.log(input);
                            var pathReference = storage.child('images/' + input);
                            pathReference.getDownloadURL().then(function (url) {
                                console.log(url);
                                $show_userimage.attr("src", url);
                            })
                            if (data.userintro) {
                                console.log("change intro");
                                $show_userintro.html(data.userintro);
                            } else {
                                $show_userintro.html("You haven't set you introduction!");
                            }
                        }
                    });

                })
                .catch(function (error) {
                    console.log(error);
                });


            /*change profile */
            $change.on("click", function () {

                var html_form = "<img id='preview_img' src='#' class='profile_image' /><input type='file' onchange='readURL(this)' targetID='preview_img' accept='image/gif, image/jpeg, image/png' id='upload_image'/>";
                var html_input = "<h3>username : <input id='username' class='text'></h3> <h3>introduction : <input id='userintro' class='text'></h3><br>"
                var html_button = "<button id='save_profile' onclick='save_profile();'>save profile</button> ";
                $profile.html(html_form + html_input + html_button);
            });

        } else {
            $profile.html("<img src='images/default-user.jpg' class='profile_image'><h3>Oops! You have to login to see you profile</h3>");
        }
    });
})(jQuery);

function readURL(input) {
    if (input.files && input.files[0]) {
        var imageTagID = input.getAttribute("targetID");
        var reader = new FileReader();
        reader.onload = function (e) {
            var img = document.getElementById(imageTagID);
            img.setAttribute("src", e.target.result)
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function readImage(input) {
    if (input.files && input.files[0]) {
        var FR = new FileReader();
        FR.onload = function (e) {
            $('#show_userimage').attr("src", e.target.result);
        };
        FR.readAsDataURL(input.files[0]);
    }
}


/*change profile*/
function change_profile() {
    var $profile = $('#profile');
    var html_form = "<img id='preview_img' src='#' class='profile_image' /><input type='file' onchange='readURL(this)' targetID='preview_img' accept='image/gif, image/jpeg, image/png' id='upload_image'/>";
    var html_input = "<h3>username : <input id='username' class='text' value=''></h3> <h3>introduction : <input id='userintro' class='text' value=''></h3><br>"
    var html_button = "<button id='save_profile' onclick='save_profile();'>save profile</button> ";

    $profile.html(html_form + html_input + html_button);
}

/*save profile */
function save_profile() {
    console.log("save profile");
    /*image saving*/
    var storageRef = firebase.storage().ref();
    var userlist = firebase.database().ref().child('userlist');
    var $profile = $('#profile'),
        btn = $('#loginbtn'),
        $show_username = $('#show_username'),
        $show_userintro = $('#show_userintro');
    var useremail = global_user.email;
    var downloadURL = false;

    var upload_image = document.getElementById('upload_image');
    var user_image = upload_image.files[0];
    var input_email = global_user.useremail.replace("@", "-");
    input_email = input_email.replace(".", "-");
    input_email = input_email.replace(".", "-");
    console.log(input_email);

    if (user_image) {
        var uploadTask = storageRef.child('images/' + input_email + '/').put(user_image);
        var new_name = $('#username')[0].value;
        var new_intro = $('#userintro')[0].value;

        uploadTask.then(function (snapshot) {
            downloadURL = snapshot.downloadURL;
            console.log(downloadURL);

            var user_data = {
                username: new_name,
                useremail: global_user.useremail,
                userintro: new_intro,
                userimage: downloadURL
            }
            var key = global_key.key;
            console.log(user_data);
            userlist.child('' + key).update(user_data).then(function () {
                if (Notification.permission == "granted") {
                    var notify = {
                        body: 'Change profile Successfully!',
                        icon: "https://drive.google.com/open?id=1YdQPZ8qxX7g5DTVw5efU7A7vpqwLbeuM"
                    };
                    var new_note = new Notification("PingMe", notify);
                }
            });
        }).then(function(){
            location.reload();
        });
    }
}

/*  change form
    <form action="/somewhere/to/upload" enctype="multipart/form-data">
        <input type="file" onchange="readURL(this)" targetID="preview_img" accept="image/gif, image/jpeg, image/png"
        />
        <img id="preview_img" src="#" style="width:100px;height:100px;" />
    </form>
    <input id="username" class="text">
    <input id="userintro" class="text">
    <br>
    <button id='save_profile'>save profile</button> 
*/

/* deploy form
    <img class='profile_image'>
    <h2 id="show_username"></h2>
    <p id="show_userintro"></p>
    <br>
    <button id="change_profile">change profile</button>
*/