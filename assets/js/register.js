$(function() {

    var $body = $('body');

    // Menu.
    $('#menu')
        .append('<a href="#menu" class="close"></a>')
        .appendTo($body)
        .panel({
            delay: 500,
            hideOnClick: true,
            hideOnSwipe: true,
            resetScroll: true,
            resetForms: true,
            side: 'right'
        });
});

$(function() {

    var database = firebase.database().ref();
    var userlist = database.child('userlist');

    //Email/Pwd註冊
    var username = document.getElementById("inputuserid");
    var account = document.getElementById("inputEmail");
    var pwd = document.getElementById("inputPassword");
    var registerSmtBtn = document.getElementById("Registerbtn");
    
    registerSmtBtn.addEventListener("click", function() {
        console.log(account.value);
        firebase.auth().createUserWithEmailAndPassword(account.value, pwd.value)
        .then(function(result){
            var data = {
                username : username.value,
                useremail : account.value
            }
            userlist.push(data);
            window.location.href = "signin.html";
        })
        .catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMsg = error.message;
            console.log(errorMsg);
        });
    }, false);

    
    //Email驗證
    /*
    var verifyBtn = document.getElementById("verifyBtn");
    verifyBtn.addEventListener("click", function() {
        user.sendEmailVerification().then(function() {
            console.log("驗證信寄出");
        }, function(error) {
            console.error("驗證信錯誤");
        });
    }, false);
    */

    //更改密碼
    /*
    var chgPwd = document.getElementById("chgPwd");
    var chgPwdBtn = document.getElementById("chgPwdBtn");
    chgPwdBtn.addEventListener("click", function() {
        firebase.auth().sendPasswordResetEmail(chgPwd.value).then(function() {
            // Email sent.
            console.log("更改密碼Email已發送");
            chgPwd.value = "";
        }, function(error) {
            // An error happened.
            console.error("更改密碼", error);
        });
    }, false);
    */
    //查看目前登入狀況
    /*
    var user;
    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            user = user;
            console.log("User is logined", user)
        } else {
            user = null;
            console.log("User is not logined yet.");
        }
    });
*/
    //如果使用者操作了更改密碼、刪除帳號、更改信箱等，需要再次驗證
    /*var user = firebase.auth().currentUser;
    var credential = firebase.auth().EmailAuthProvider.credential(
    	user.email,
      //password from user
    )*/
});