# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [PingMe]
* Key functions (add/delete)
    1. Chatroom
    2. Change chatroom
    3. Account

    
* Other functions (add/delete)
    1. Notification
    2. Users' Profile
    3. User list
    4. CSS animation
    5. List
    6. RWD
    7. Footer
    


## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description

# 作品網址：https://105062323.gitlab.io/Midterm_Project/index.html
# 報告網址：https://gitlab.com/105062323/Midterm_Project/blob/master/README.md

# 主要功能 : 
1. Chatroom : 可以讓多個user傳送訊息到同一個聊天室，且訊息附有時間戳記。
2. Change chatroom : 模仿跑跑卡丁車的聊天室模式。每個user都有自己的聊天室，別的user可以透過輸入ID加入其聊天室。
3. Account : 可以透過email註冊，或是利用google登入，登入後menu也會新增logout讓user可以登出。

# 其他功能 : 
1. Notification : 當user登入/登出或更換profile時，會發出notification。
2. Users' Profile : 有user的個人檔案，可以更換名字、自介、大頭貼。
3. User list : 有所有user的列表，可以透過這個列表觀看有哪些user的聊天室可以加入
4. CSS animation : 房間名字滑動 & 首頁頁面定時切換
5. List : 實作了三種menu跳出的方式(room、menu、userlist)
6. RWD : 會隨著視窗大小去調整版面配置。
7. Footer : 固定式底標，可以貼上相關的連結。

## Security Report (Optional)

網站無法隱藏JS檔，所以config.js裡的資料會透露出去
因此，為了不讓其他不明網站或程式任意更改database
所以在權限的部分設定需要改成auth!==null，讀跟寫都是

另外，也在網站的部分經由firebase.auth認證通過後，確認為合法使用者才可以使用內部的功能

除此之外，在auth授權網域部分也要把預設的localhost移除
才不會讓其他人可以自己寫另一個網站來建立新的帳戶進來亂改資訊

text input的部分則把全部的"<"替換成"/"，這樣就不會有message裡有html code的風險